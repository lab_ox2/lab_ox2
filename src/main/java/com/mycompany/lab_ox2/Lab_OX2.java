/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab_ox2;

/**
 *
 * @author ASUS
 */
public class Lab_OX2 {

    static char[][] table = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};

    public static void main(String[] args) {
        printWelcome();
        printTable();
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    private static void printTable() {
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                System.out.print(table[i][j]+ " ");
            }
            System.out.println("");
        }
        
    }
}
